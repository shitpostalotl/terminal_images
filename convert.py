from PIL import Image
pic = Image.open(input())
width, height = pic.size

for i in range(0, height):
    buffer = ""
    for j in range(0, width):
        r, g, b = pic.convert('RGB').getpixel((j, i))
        buffer = buffer + "\033[38;2;{};{};{}m█ \033[38;2;255;255;255m".format(r, g, b)
    print(buffer)
# 9 by 17